;
(function ($, window, undefined) {
  'use strict';
  // code using $ as alias to jQuery

  // document.ready shorthand
  $(function () {

    var PhoneApp = PhoneApp || {};

    PhoneApp.utils = (function () {
      var self = this;

      ///////////////
      return {}
    }());

    PhoneApp.dnaForm = (function () {
      function init() {
        var $trigger = $('.ph--nav-bar_dna'),
          $form = $(".ph--dna-form");

        $trigger.on('click', function () {
          $(this).closest('.ph--dna-helper').toggleClass('down')
        });

        $form.find('[data-next]').on('click', function () {
          $form.find('[href=' + $(this).data('next') + ']').tab('show');
        })
      }

      return {
        init: init,
      }

    }())

    PhoneApp.datePicker = (function () {
      function init() {
        $('.datetimepicker').each(function (index, item) {
          var $item = $(item);
          $item.closest('.form-group').addClass('datetimepicker-wrap');
          $item.datetimepicker({
            // debug: true,
            widgetPositioning: {
              vertical: $item.data('vertical') || 'auto',
            },
            format: $item.data('format') || undefined,
            viewMode: $item.data('view') || undefined,
          })
        });
      }

      return {
        init: init,
      }

    }())

    PhoneApp.accountSwitch = (function () {
      var self = this;

      function init() {
        var $account = $('.ph--account'),
          $triggers = $account.find('.ph--account-nav a');

        $triggers.on('shown.bs.tab', function (e) {
          $account.attr('data-current', e.target.hash);
        })
      }

      ///////////////
      return {
        init: init,
      }
    }());


    PhoneApp.autoComplete = (function () {
      var data = {
        drugs: [{
            id: "someId1",
            name: "Ibudone"
          },
          {
            id: "someId2",
            name: "Ibuprofen"
          },
          {
            id: "someId3",
            name: "Ibuprofen Drops"
          },
          {
            id: "someId8",
            name: "example"
          },
          {
            id: "someId4",
            name: "pharma"
          }
        ],
        dosage: [{
            id: "someId1",
            name: "100 mg Tablet"
          },
          {
            id: "someId2",
            name: "200 mg Tablet"
          },
          {
            id: "someId3",
            name: "300 mg Tablet"
          },
          {
            id: "someId8",
            name: "400 mg Tablet"
          },
          {
            id: "someId4",
            name: "5000 mg Tablet"
          }
        ]
      };

      function init() {
        var $input = $(".typeahead");
        $input.each(function (index, item) {
          var $item = $(item);
          $item.typeahead({
            source: data[$item[0].dataset.source],
            showHintOnFocus: true,
            autoSelect: true
          });
          $item.change(function () {
            var current = $item.typeahead("getActive");
            if (current) {
              // Some item from your model is active!
              if (current.name == $item.val()) {
                // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
              } else {
                // This means it is only a partial match, you can either add a new item
                // or take the active if you don't want new items
              }
            } else {
              // Nothing is active so it is a new value (or maybe empty value)
            }
          });
        })

      }

      ///////////////
      return {
        init: init,
      }
    }());


    PhoneApp.customSelect = (function () {

      function init() {}

      ///////////////
      return {
        init: init,
      }
    }());

    PhoneApp.tabs = (function () {
      function init() {
        $("ul.nav-tabs a").click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        });
      }
      ///////////////
      return {
        init: init,
      }
    }())

    PhoneApp.init = (function () {
      var self = this;

      function init() {
        PhoneApp.accountSwitch.init();
        PhoneApp.autoComplete.init();
        PhoneApp.customSelect.init();
        PhoneApp.tabs.init();
        PhoneApp.datePicker.init();

        if ($('.ph--nav-bar_dna').get(0)) {
          PhoneApp.dnaForm.init();
        }
      }

      ///////////////
      return init
    }());

    PhoneApp.init();

  });
  // document.ready shorthand end

})(jQuery, window, undefined);
// code using $ as alias to jQuery end
