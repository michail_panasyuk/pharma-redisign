'use strict';

var gulp = require('gulp'),
  watch = require('gulp-watch'),
  autoprefixer = require('autoprefixer'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  rigger = require('gulp-rigger'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  rimraf = require('rimraf'),
  browserSync = require("browser-sync"),
  spritesmith = require('gulp.spritesmith'),
  buffer = require('vinyl-buffer'),
  postcss = require('gulp-postcss'),
  plumber = require('gulp-plumber'),
  notify = require('gulp-notify'),
  panini = require('panini'),

  reload = browserSync.reload,
  rollup = require('rollup'),
    resolve = require('rollup-plugin-node-resolve'),
    babel = require('rollup-plugin-babel'),


    //deploy
    surge = require('gulp-surge')


var processors = [
  autoprefixer({
    browsers: ['last 3 version']
  })
];

var plumberErrorHandler = {
  errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};
var buildAlias = './build/',
  srcAlias = './src/';

var path = {
  build: {
    html: buildAlias + '',
    js: buildAlias + 'js/',
    css: buildAlias + 'css/',
    img: buildAlias + 'img/',
    sprites: buildAlias + 'sprites/',
    fonts: buildAlias + 'fonts/',
    voice: buildAlias + 'voice/',
    video: buildAlias + 'video/'
  },
  src: {
    html: srcAlias + 'pages/**/*.html',
    js: srcAlias + 'js/**/*.js',
    style: srcAlias + 'scss/app.scss',
    img: srcAlias + 'img/**/*.*',
    fonts: srcAlias + 'fonts/**/*.*',
    sprites: srcAlias + 'sprites/**/*.*',
    voice: srcAlias + 'voice/**/*.*',
    video: srcAlias + 'video/**/*.*'
  },
  watch: {
    panini: ['${path.srcAlias}/{layouts,partials,helpers,data}/**/*.html'],
    html: srcAlias + 'pages/**/*.html',
    js: srcAlias + 'js/**/*.js',
    style: srcAlias + 'scss/**/*.scss',
    sprites: srcAlias + 'sprites/**/*.*',
    img: srcAlias + 'img/**/*.*',
    fonts: srcAlias + 'fonts/**/*.*'
  },
  clean: buildAlias,
};

var config = {
  server: {
    baseDir: buildAlias,
  },
  // tunnel: true,
  host: 'localhost',
  port: 9000,
  logPrefix: "t3"
};

gulp.task('html:build', function () {
  return gulp.src(path.src.html)
    .pipe(panini({
      root: srcAlias + 'pages/',
      layouts: srcAlias + 'layouts/',
      partials: srcAlias + 'partials/',
      helpers: srcAlias + 'helpers/',
      data: srcAlias + 'data/'
    }))
    .pipe(gulp.dest(buildAlias))
    .pipe(reload({
      stream: true
    }))
});

gulp.task('style:build', function () {
  gulp.src(path.src.style)
    // .pipe(sourcemaps.init())
    .pipe(plumber(plumberErrorHandler))
    .pipe(sass({
      errLogToConsole: false
    }))
    .pipe(postcss(processors))
    // .pipe(cssmin()) //Сожмем
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({
      stream: true
    }));
});

gulp.task('js:build', function () {
  return gulp.src(path.src.js)
    .pipe(plumber(plumberErrorHandler))
    // .pipe(rigger())
    // .pipe(sourcemaps.init())
    // .pipe(uglify())
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({
      stream: true
    }));


//   return rollup.rollup({
//     entry: 'src/js/app.js',
//     plugins: [
//       resolve({
//         // pass custom options to the resolve plugin
//         customResolveOptions: {
//           moduleDirectory: 'node_modules'
//         },
//       }),
//       babel({
//         presets: [
//           [
//             "es2015", {
//               "modules": false
//             }
//           ]
//         ],
//         babelrc: false,
//         exclude: 'node_modules/**'
//       })
//     ],
//     external: ['bootstrap-sass'],
//   })
//   .then(function (bundle) {
//       bundle.write({
//         dest: "./build/js/app.js",
//         // indicate which modules should be treated as external
        
//         sourceMap: true
//       });
//     })
//     .then(() => {
//         reload({
//             stream: true
//         })
//     })
});

gulp.task('sprite:build', function () {
  var spriteData = gulp.src(path.src.sprites).pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.scss',
    padding: 2
  }));
  var imgStream = spriteData.img.pipe(gulp.dest(path.build.sprites));

  var cssStream = spriteData.css.pipe(gulp.dest('src/scss/'));

});

gulp.task('image:build', function () {
  gulp.src(path.src.img)
    .pipe(plumber(plumberErrorHandler))
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{
        removeViewBox: false
      }],
      use: [pngquant()],
      interlaced: true
    }))
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({
      stream: true
    }));
});

gulp.task('fonts:build', function () {
  gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

gulp.task('voice:build', function () {
  gulp.src(path.src.voice)
    .pipe(gulp.dest(path.build.voice))
});

gulp.task('video:build', function () {
  gulp.src(path.src.video)
    .pipe(gulp.dest(path.build.video))
});

gulp.task('build', [
  'html:build',
  'js:build',
  'style:build',
  'fonts:build',
  'voice:build',
  'video:build',
  'image:build',
  'sprite:build'
]);

gulp.task('watch', function () {
  watch([path.watch.html], function (event, cb) {
    gulp.start('html:build');
    panini.refresh();
  });
  watch([path.watch.style], function (event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function (event, cb) {
    gulp.start('js:build');
  });
  watch([path.watch.sprites], function (event, cb) {
    gulp.start('sprite:build');
  });
  watch([path.watch.img], function (event, cb) {
    gulp.start('image:build');
  });
  watch([path.watch.fonts], function (event, cb) {
    gulp.start('fonts:build');
  });
});

gulp.task('webserver', function () {
  browserSync(config);
});

gulp.task('clean', function (cb) {
  rimraf(path.clean, cb);
});

gulp.task('deploy', [], function () {
  return surge({
    project: './build',         // Path to your static build directory
    domain: 'pharmazam.surge.sh'  // Your domain or Surge subdomain
  })
})
gulp.task('deploy-small', [], function () {
  return surge({
    project: './build',         // Path to your static build directory
    domain: 'pharmazam-small.surge.sh'  // Your domain or Surge subdomain
  })
})

gulp.task('default', ['build', 'webserver', 'watch']);

// gulp.task('default', ['build', 'watch']);
